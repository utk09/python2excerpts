# This code will show you your latest tweet on twitter.
# You need Twitter API for this, visit - https://apps.twitter.com
# Sign in with your Twitter Account, fill in the required details and generate Access Token.
# Use the consumer and access tokens in place of asterisk (***).
# Note: Do not share your secret keys with anyone!


# Requirement - pip install python-twitter
# If yor're using PyCharm then go to File -- Settings -- Project -- Project Interpreter -- Click on "+" icon & search "python-twitter"

import sys, twitter

# Not required, but you can uncomment it.
#api = twitter.Api()

# Populate your twitter API details below

consumer_key = '*******************'
consumer_secret = '**********************'
access_token_key = '************************'
access_token_secret = '*************************'

api = twitter.Api(
    consumer_key=consumer_key,
    consumer_secret=consumer_secret,
    access_token_key=access_token_key,
    access_token_secret=access_token_secret
)


def user_tweet(thandle):
    statuses = api.GetUserTimeline(screen_name=thandle)
    return statuses[0].text


if __name__ == "__main__":

# This line was not working

    # latest_tweet = user_tweet(sys.argv[1])
    
# So changed to this

    latest_tweet = user_tweet(sys.argv[1] if len(sys.argv) > 1 else 0)
    print latest_tweet
    
# It'll just print you latest (only 1) tweet.
