# python2excerpts
Random Python Codes which are actually helpful :)

## All these codes have been tested for Python 2
For Python 3, check out my other repository of Python 3

# What is this about?
These are some random codes, some I came across, some I wrote myself and some I'm still working on!
You're free to test them, email me too in case they are not working as expected or you found a better way!
Thank You.
